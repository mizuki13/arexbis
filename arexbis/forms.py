from django import forms
from .models import *
from datetime import datetime

FORMATIONS = [
    ("BUT2", "BUT2"),
    ("BUT3", "BUT3")
]

class ConventionStageForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ConventionStageForm, self).__init__(*args, **kwargs)
        self.fields['dateDebut'].label = "Date de début"
        self.fields['dateFin'].label = "Date de fin"
        self.fields['mission'].label = "Missions"
        self.fields['volhebdo'].label = "Volume hebdomadaire"
        self.fields['grathoraire'].label = "Gratification horaire (obligatoire si stage supérieur à 44 jours)"
        self.fields['entreprise'].queryset = Entreprise.objects.all()
        self.fields['prenomMaitreStage'].label = "Prénom du maître de stage"
        self.fields['nomMaitreStage'].label = "Nom du maître de stage"

    class Meta:
        model = ConventionStage
        fields = '__all__'
        widgets = {
            'dateDebut': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'dateFin': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'stagiaire': forms.HiddenInput(),
            'gestionnaire': forms.HiddenInput(),
            'etat': forms.HiddenInput(),
        }

class EntrepriseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EntrepriseForm, self).__init__(*args, **kwargs)
        self.fields['nomEnt'].label = "Nom de l'entreprise"
        self.fields['tel'].label = "Numéro de téléphone"
        self.fields['adresse'].label = "Adresse"
        self.fields['ville'].label = "Ville"
        self.fields['mailContact'].label = "Mail de contact"

    class Meta:
        model = Entreprise
        fields = '__all__'
        widgets = {
            'mailContact' : forms.EmailInput(attrs={'type':'email'}),
        }

class SoutenanceStageForm(forms.ModelForm):
    def __init__(self, *args, stagiaire=None, **kwargs):
        super(SoutenanceStageForm, self).__init__(*args, **kwargs)
        self.fields['date'].label = "Date"
        self.fields['heure'].label = "Heure"
        self.fields['salle'].label = "Salle"
        self.fields['salle'].queryset = Salle.objects.all()
        self.fields['candide'].label = "Candide"
        self.fields['candide'].queryset = Candide.objects.all()

    class Meta :
        model = SoutenanceStage
        fields = '__all__'
        widgets = {
            'date' : forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'heure' : forms.TimeInput(attrs={'type': 'time'}, format='%H:%M'),
            'stage' : forms.HiddenInput(),
        }

class SoutenanceApprentissageForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SoutenanceApprentissageForm, self).__init__(*args, **kwargs)
        self.fields['date'].label = "Date"
        self.fields['heure'].label = "Heure"
        self.fields['salle'].label = "Salle"
        self.fields['salle'].queryset = Salle.objects.all()
        self.fields['candide'].label = "Candide"
        self.fields['candide'].queryset = Candide.objects.all()

    class Meta :
        model = SoutenanceApprentissage
        fields = '__all__'
        widgets = {
            'date' : forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'heure' : forms.TimeInput(attrs={'type': 'time'}, format='%H:%M'),
            'apprentissage' : forms.HiddenInput(),
        }
        
class OffreForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(OffreForm, self).__init__(*args, **kwargs)
        self.fields['entreprise'].queryset = Entreprise.objects.all()
        self.fields['intitule'].label = "Intitulé"
        self.fields['formationVisee'].label = "Formation visée par l'offre"
        self.fields['projet'].label = "Offre de stage ou d'apprentissage ?"

    class Meta:
        model = Offre
        fields = '__all__'
        widgets = {
            'datePubli' : forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'datePubli' : forms.HiddenInput(),
        }

        
