from django.contrib import admin
from arexbis.models import *

# Register your models here.

FORMATIONS = [
    ("BUT2", "BUT2"),
    ("BUT3", "BUT3")
]

TRAITEMENT = [ 
    ("transmis", "transmis"),
    ("signé", "signé")
]

class StagiaireAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement',
        'adresse',
        'ville',
        'tel',
        'formation',
    )

class ApprentiAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement',
        'adresse',
        'ville',
        'tel',
        'formation',
    )

class CandideAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement',
    )

class TuteurAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement',
    )
        
class GestionnaireAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement',
        'projet',
        'niveau',
    )

class SecretaireAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'departement'
    )

class EntrepriseAdmin(admin.ModelAdmin):
    list_display = (
        'nomEnt',
        'tel',
        'adresse',
        'ville',
        'mailContact',
    )

class OffreAdmin(admin.ModelAdmin):
    list_display = (
        'datePubli',
        'intitule',
        'formationVisee',
        'entreprise',
        'projet',
    )

class ConventionStageAdmin(admin.ModelAdmin):
    list_display = (
        'dateDebut',
        'dateFin',
        'mission',
        'stagiaire',
        'tuteur',
        'etat',
        'gestionnaire',
        'signatureMaitreStage',
        'signatureGestionnaire',
        'signatureStagiaire',
        'signatureDirecteur',
        'validationSecretariat',
        'entreprise',
        'prenomMaitreStage',
        'nomMaitreStage'
    )

class SalleAdmin(admin.ModelAdmin):
    list_display = ( 
        'nomSalle', 
    )

class SoutenanceStageAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'heure',
        'salle',
        'candide',
        'stage',
    )

class SoutenanceApprentissageAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'heure',
        'salle',
        'candide',
        'apprentissage',
    )

class SoutenanceStageAdmin(SoutenanceStageAdmin):
    list_display = (
        'stage',
    )

class SoutenanceApprentissageAdmin(SoutenanceApprentissageAdmin):
    list_display = (
        'apprentissage',
    )

class ContratApprentissageAdmin(admin.ModelAdmin):
    list_display = (
        'apprenti',
        'tuteur',
        'gestionnaire',
        'dateDebut',
        'dateFin',
        'mission',
        'grathoraire',
        'volhebdo',
        'entreprise',
        'prenomMaitreApprentissage',
        'nomMaitreApprentissage'
    )

admin.site.register(Gestionnaire, GestionnaireAdmin)
admin.site.register(Secretaire, SecretaireAdmin)
admin.site.register(Candide, CandideAdmin)
admin.site.register(Tuteur, TuteurAdmin)
admin.site.register(Offre, OffreAdmin)
admin.site.register(Stagiaire, StagiaireAdmin)
admin.site.register(Apprenti, ApprentiAdmin)
admin.site.register(Entreprise, EntrepriseAdmin)
admin.site.register(SoutenanceStage, SoutenanceStageAdmin)
admin.site.register(SoutenanceApprentissage, SoutenanceApprentissageAdmin)
admin.site.register(Salle, SalleAdmin)
admin.site.register(ConventionStage, ConventionStageAdmin)
admin.site.register(ContratApprentissage, ContratApprentissageAdmin)
