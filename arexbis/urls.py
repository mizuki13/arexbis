from django.conf import settings
from django.urls import path
from .views import *

app_name = "main"

urlpatterns =[
    path('', choixstatut, name="choixstatut"), # tous
    path('<statut>/login/', connexion, name='connexion'), # tous

    path('<statut>/<id>/', home, name='home'), # tous
    
    path('<statut>/<id>/moncompte/', moncompte, name="moncompte"), # tous

    path('<statut>/<id>/posteroffre/', posteroffre, name='posteroffre'), # gestionnaires secrétaires
    path('<statut>/<id>/modifoffre/<idoffre>/', modifoffre, name='modifoffre'), # gestionnaires secrétaires
    path('<statut>/<id>/supproffre/<idoffre>/', suppoffre, name='suppoffre'), # gestionnaires secrétaires
    path('<statut>/<id>/mesoffres/', offres, name="mesoffres"), # apprentis stagiaires gestionnaires secrétaires
    path('<statut>/<id>/offre/<idoffre>/', offre, name="offre"), # apprentis stagiaires gestionnaires secrétaires
    path('<statut>/<id>/offresentreprise/', offresentreprise, name='offresentreprise'), # gestionnaires secrétaires

    path('<statut>/<id>/mesdocuments/', mesdocuments, name="mesdocuments"), # apprentis stagiaires gestionnaires secrétaires tuteurs
    path('<statut>/<id>/convention/<idstage>/', convention, name="convention"), # stagiaires gestionnaires secrétaires tuteurs
    path('<statut>/<id>/contrat/<idalternance>/', visucontrat, name="visucontrat"), # apprentis gestionnaires secrétaires tuteurs
    path('<statut>/<id>/creerdocument/', creerdocument, name='creerdocument'), # stagiaires
    path('<statut>/<id>/modifconvention/<idconv>/', modifdconvention, name='modifconvention'), # stagiaires secretaires
    path('<statut>/<id>/signature/<champ>/<idconv>/', signature, name='signature'), # secretaires
    path('<statut>/<id>/ajoutertuteur/<idconv>/', ajoutertuteur, name='ajoutertuteur'), # secretaires
    path('<statut>/<id>/creationpdf/<idconv>/',creationpdf,name="creationpdf" ),

    path('<statut>/<id>/annuaire/', annuaire, name="annuaire"), # apprentis stagiaires gestionnaires secrétaires tuteurs
    path('<statut>/<id>/ajoutentreprise/', ajoutentreprise, name='ajoutentreprise'), # gestionnaires secrétaires
    path('<statut>/<id>/modifentreprise/<ident>/', modifentreprise, name='modifentreprise'), # gestionnaires secrétaires
    path('<statut>/<id>/suppentreprise/<ident>/', suppentreprise, name='suppentreprise'), # gestionnaires secrétaires
   
    path('<statut>/<id>/listesoutenancestage/',listesoutenancestage,name='listesoutenancestage'), #candides, gestionnaires, tuteurs
    path('<statut>/<id>/listesoutenanceapprenti/',listesoutenanceapprenti,name='listesoutenanceapprenti'), #candides, gestionnaires, tuteurs
    path('<statut>/<id>/choixsoutenance/', choixsoutenance, name='choixsoutenance'),
    path('<statut>/<id>/ajoutsoutenance/<statutetu>/<iddoc>/', ajoutsoutenance, name='ajoutsoutenance'), # gestionnaires
    path('<statut>/<id>/modifsoutenance/<statutetu>/<idsout>/', modifsoutenance, name='modifsoutenance'), # gestionnaires
    path('<statut>/<id>/suppsoutenance/<statutetu>/<idsout>/', suppsoutenance, name='suppsoutenance'), # gestionnaires

    path('logout/', logout_request, name="logout"),
]
