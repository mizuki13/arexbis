from django.db import models
from django.utils.html import format_html
from django.core.validators import FileExtensionValidator
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from datetime import datetime
from django.db.models import CheckConstraint, Q, F
from django.utils.translation import gettext_lazy as _

FORMATIONS = [
    ("BUT2", "BUT2"),
    ("BUT3", "BUT3")
]

DEPARTEMENT = [
    ("INFO", "INFO"),
    ("MT2E", "MT2E"),
    ("GEA", "GEA"),
    ("CHIMIE", "CHIMIE"),
    ("QLIO", "QLIO"),
    ("GMP", "GMP"),
]

PROJET = [
    ("stage", "stage"),
    ("apprentissage", "apprentissage")
]

class Utilisateur(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True) 
    # on a donc accès à username, password, email, first_name, last_name
    departement = models.CharField(max_length=6, choices=DEPARTEMENT)
    def __str__(self):
        return str(self.user.last_name + " " + self.user.first_name + " " + self.user.username)
    class Meta:
        abstract=True
  
class Etudiant(Utilisateur):
    adresse = models.TextField(max_length=128)
    ville = models.CharField(max_length=32)
    tel = models.IntegerField()
    formation = models.CharField(max_length=4, choices=FORMATIONS)
    def __str__(self):
        return super().__str__()
    class Meta:
        abstract=True

class Secretaire(Utilisateur):
    def __str__(self):
        return super().__str__()

class Stagiaire(Etudiant):
    def __str__(self):
        return super().__str__()

class Apprenti(Etudiant):
    def __str__(self):
        return super().__str__()

class Enseignant(Utilisateur):
    def __str__(self):
        return super().__str__()
    class Meta:
        abstract=True

class Candide(Enseignant):
    def __str__(self):
        return super().__str__()

class Tuteur(Enseignant):
    def __str__(self):
        return super().__str__()

class Gestionnaire(Enseignant):
    niveau = models.CharField(max_length=4, choices=FORMATIONS)
    projet = models.CharField(max_length=13, choices=PROJET)
    def __str__(self):
        return super().__str__()
    class Meta:
        unique_together = ('niveau', 'projet', 'departement')

class Entreprise(models.Model):
    nomEnt = models.CharField(max_length=32, unique=True)
    tel = models.IntegerField()
    adresse = models.TextField(max_length=128)
    ville = models.CharField(max_length=32)
    mailContact = models.EmailField(max_length=256)
    def __str__(self):
        return self.nomEnt + " " + self.ville

class Offre(models.Model):
    datePubli = models.DateField()
    intitule = models.TextField(max_length=256)
    formationVisee = models.CharField(max_length=4, choices=FORMATIONS)
    entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)
    projet = models.CharField(max_length=13, choices=PROJET)
    def __str__(self):
        return str(self.entreprise) + ' : ' + self.intitule + ' ; ' + self.projet

class Salle(models.Model):
    nomSalle = models.CharField(max_length=16)
    def __str__(self):
        return self.nomSalle

class ConventionStage(models.Model):
    signatureMaitreStage = models.BooleanField(default=False) # signature validée par le secrétariat
    signatureGestionnaire = models.BooleanField(default=False) # signature validée par le secrétariat
    signatureStagiaire = models.BooleanField(default=False) # signature validée par le secrétariat
    signatureDirecteur = models.BooleanField(default=False) # signature validée par le secrétariat
    validationSecretariat = models.BooleanField(default=False)
    etat = models.CharField(max_length=32, default="en cours de traitement")
    dateDebut = models.DateField()
    dateFin = models.DateField()
    mission = models.TextField()
    volhebdo = models.DecimalField(decimal_places=2, max_digits=5)
    grathoraire = models.DecimalField(decimal_places=2, max_digits=5)
    gestionnaire = models.ForeignKey(Gestionnaire, on_delete=models.CASCADE)
    stagiaire = models.ForeignKey(Stagiaire, on_delete=models.CASCADE)
    tuteur = models.ForeignKey(Tuteur, on_delete=models.CASCADE, null=True, blank=True)
    entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)
    nomMaitreStage = models.CharField(max_length=32)
    prenomMaitreStage = models.CharField(max_length=32)
    def __str__(self):
        return str(self.stagiaire)+' '+str(self.dateDebut)+' '+str(self.entreprise)
    class Meta:
        constraints = [
            # Ensures constraint on DB level, raises IntegrityError (500 on debug=False)
            CheckConstraint(
                check=Q(dateFin__gt=F('dateDebut')), name='dates valides',
            ),
        ]

class ContratApprentissage(models.Model):
    dateDebut = models.DateField()
    dateFin = models.DateField()
    mission = models.TextField()
    volhebdo = models.DecimalField(decimal_places=2, max_digits=5)
    grathoraire = models.DecimalField(decimal_places=2, max_digits=5)
    gestionnaire = models.ForeignKey(Gestionnaire, on_delete=models.CASCADE)
    apprenti = models.ForeignKey(Apprenti, on_delete=models.CASCADE)
    tuteur = models.ForeignKey(Tuteur, on_delete=models.CASCADE, null=True, blank=True)
    entreprise = models.ForeignKey(Entreprise, on_delete=models.CASCADE)
    nomMaitreApprentissage = models.CharField(max_length=32)
    prenomMaitreApprentissage = models.CharField(max_length=32)
    def __str__(self):
        return str(self.apprenti)+' '+str(self.entreprise)

class Soutenance(models.Model):
    date = models.DateField()
    heure = models.TimeField()
    salle = models.ForeignKey(Salle, on_delete=models.CASCADE)
    candide = models.ForeignKey(Candide, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.date)
    class Meta :
        abstract = True

class SoutenanceStage(Soutenance):
    stage = models.OneToOneField(ConventionStage, on_delete=models.CASCADE)
    def __str__(self):
        return super().__str__()
    
class SoutenanceApprentissage(Soutenance):
    apprentissage = models.ForeignKey(ContratApprentissage, on_delete=models.CASCADE)
    def __str__(self):
        return super().__str__()



