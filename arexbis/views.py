from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse, redirect

from .forms import *
from .models import *
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.core.handlers.wsgi import WSGIRequest
from datetime import date, datetime, timedelta
from django.template.loader import get_template
from xhtml2pdf import pisa


# Create your views here.

# année scolaire du document sous format string "YYYY/YYYY" (si pas de paramètre transmis alors année scolaire actuelle)
def anneeScolaire(date=datetime.today()) :
    month = date.strftime("%m")
    year = date.strftime("%Y")
    if int(month)>=9 :
        annee = year + "/" + str(int(year)+1)
    else :
        annee = str(int(year)-1) + "/" + year
    return annee

# renvoie true si le doc en paramètre est actuel, false sinon
def docActuel(doc) :
    return anneeScolaire(doc.dateDebut)==anneeScolaire()

def champHidden(form, champ) :
    field = form.fields[champ]
    field.widget = field.hidden_widget()

def champsHidden(form, champs) :
    for champ in champs :
        champHidden(form, champ)

# form de convention et de contrat
# création dispo pour apprentis et stagiaires
# modification dispo pour gestionnaires, tuteurs + case à cocher pour leur propre signature
# les secrétaires peuvent y cocher la case signatureMaitreStage, signatureDirecteur et ajouter un tuteur une fois que le directeur a signé
# modification des champs dispo pour les stagiaires tant que pas de signature
def creerdocument(request: WSGIRequest, statut="", id=""):
    user = User.objects.get(username=id)
    stagiaire = Stagiaire.objects.get(user=user)
    gestionnaire = Gestionnaire.objects.get(departement=stagiaire.departement, niveau=stagiaire.formation, projet="stage")
    form = ConventionStageForm(initial={'stagiaire':stagiaire , 'gestionnaire':gestionnaire, 'grathoraire':4.05})
    champsHidden(form, ['tuteur', 'stagiaire', 'gestionnaire', 'signatureDirecteur', 'signatureStagiaire', 'signatureMaitreStage', 'signatureGestionnaire', 'validationSecretariat'])
    if request.method == 'POST':
        form = ConventionStageForm(request.POST)
        if form.is_valid():
            convention = form.save()
            return redirect('/arexbis/' + statut + '/' + id + '/mesdocuments/')
    context = {'form': form}
    return render(request, "remplirdocument.html", context)

def modifdconvention(request: WSGIRequest, statut="", id="", idconv=""):
    convention = ConventionStage.objects.get(id=idconv)
    form = ConventionStageForm(instance=convention)
    champsHidden(form, ['tuteur', 'stagiaire', 'gestionnaire', 'signatureDirecteur', 'signatureStagiaire', 'signatureMaitreStage', 'signatureGestionnaire'])
    if statut != "secretaire" :
        champHidden(form, 'validationSecretariat')
    if request.method == 'POST':
        form = ConventionStageForm(request.POST, instance=convention)
        if form.is_valid():
            conv = form.save()
            return redirect('/arexbis/' + statut + '/' + id + '/mesdocuments/')
    context = {'form': form}
    return render(request, "remplirdocument.html", context)

# affiche le stage en question (stagiaires, tuteurs, gestionnaires)
def convention(request: WSGIRequest,id="",statut="",idstage=""):
    convention=ConventionStage.objects.get(id=idstage)
    if (statut == "stagiaire" and not convention.validationSecretariat) or (statut == "secretaire" and not convention.signatureStagiaire and not convention.validationSecretariat) :
        return redirect('/arexbis/'+statut+'/'+id+'/modifconvention/'+idstage+'/')
    if statut == "secretaire" :
        if not convention.signatureStagiaire :
            return redirect('/arexbis/'+statut+'/'+id+'/signature/signatureStagiaire/'+idstage+'/')
        if not convention.signatureGestionnaire :
            return redirect('/arexbis/'+statut+'/'+id+'/signature/signatureGestionnaire/'+idstage+'/')
        if not convention.signatureMaitreStage :
            return redirect('/arexbis/'+statut+'/'+id+'/signature/signatureMaitreStage/'+idstage+'/')
        if not convention.signatureDirecteur :
            return redirect('/arexbis/'+statut+'/'+id+'/signature/signatureDirecteur/'+idstage+'/')
        if convention.tuteur == None :
            return redirect('/arexbis/'+statut+'/'+id+'/ajoutertuteur/'+idstage+'/')
    context={"iduser":id,"convention":convention,"statut":statut}
    return render(request,'convention.html',context)

def signature(request: WSGIRequest, statut="", id="", idconv="", champ="") :
    convention = ConventionStage.objects.get(id=idconv)
    form = ConventionStageForm(instance=convention)
    champs = ['dateDebut', 'dateFin', 'mission', 'nomMaitreStage', 'prenomMaitreStage', 'grathoraire', 'volhebdo', 'entreprise', 'tuteur', 'stagiaire', 'gestionnaire', 'signatureDirecteur', 'signatureStagiaire', 'signatureMaitreStage', 'signatureGestionnaire', 'validationSecretariat']
    champs.remove(champ)
    champsHidden(form, champs)
    if request.method == 'POST':
        form = ConventionStageForm(request.POST, instance=convention)
        if form.is_valid():
            conv = form.save()
            if champ == "signatureDirecteur" :
                conventionValidee(idconv)
            return redirect('/arexbis/' + statut + '/' + id + '/convention/'+idconv+'/')
    context = {'form': form, 'convention':convention}
    return render(request, "convention.html", context)

def conventionValidee(idconv) :
    convention = ConventionStage.objects.get(id=idconv)
    convention.etat = "validee"
    convention.save()
    return

def ajoutertuteur(request: WSGIRequest,statut="",id="", idconv="") :
    convention = ConventionStage.objects.get(id=idconv)
    stagiaire = convention.stagiaire
    form = ConventionStageForm(instance=convention)
    form.fields['tuteur'].queryset = Tuteur.objects.all().filter(departement=stagiaire.departement)
    champs = ['dateDebut', 'dateFin', 'mission', 'nomMaitreStage', 'prenomMaitreStage', 'grathoraire', 'volhebdo', 'entreprise', 'tuteur', 'stagiaire', 'gestionnaire', 'signatureDirecteur', 'signatureStagiaire', 'signatureMaitreStage', 'signatureGestionnaire', 'validationSecretariat']
    champs.remove('tuteur')
    champsHidden(form, champs)
    if request.method == 'POST':
        form = ConventionStageForm(request.POST, instance=convention)
        if form.is_valid():
            conv = form.save()
            return redirect('/arexbis/' + statut + '/' + id + '/convention/'+idconv+'/')
    context = {'form': form, 'convention':convention}
    return render(request, "convention.html", context)

# affichage de toutes les entreprises de la bd (gestionnaires, stagiaires, tuteurs, apprentis, secretaires)
def annuaire(request: WSGIRequest,statut="",id=""):
    entreprises=Entreprise.objects.all().order_by("ville")
    context={"entreprises":entreprises, "statut":statut,"iduser":id }
    return render(request, "annuaire.html",context)

# form d'ajout d'entreprise à l'annuaire
# dispo pour gestionnaires et secrétaires
def ajoutentreprise(request: WSGIRequest, statut="", id=""):
    if request.method == 'POST':
        form = EntrepriseForm(request.POST)
        if form.is_valid():
            entreprise = form.save()
            return redirect("/arexbis/"+statut+"/"+id+"/annuaire")
    form = EntrepriseForm()
    context = {'form': form}
    return render(request, "ajoutentreprise.html", context)

# dispo pour gestionnaires et secrétaires
def modifentreprise(request: WSGIRequest, statut="", id="", ident=""):
    entreprise = Entreprise.objects.get(id=ident)
    if request.method == 'POST':
        form = EntrepriseForm(request.POST, instance=entreprise)
        if form.is_valid():
            new_ent = form.save()
            return redirect("/arexbis/"+statut+"/"+id+"/annuaire")
    form = EntrepriseForm(instance=entreprise)
    context = {'form': form}
    return render(request, 'ajoutentreprise.html', context)

# dispo pour gestionnaires et secrétaires
def suppentreprise(request: WSGIRequest, statut="", id="", ident=""):
    Entreprise.objects.get(id=ident).delete()
    return redirect("/arexbis/"+statut+"/"+id+"/annuaire/")

# dispo pour gestionnaires
def ajoutsoutenance(request: WSGIRequest, statut="", id="", statutetu="", iddoc=""):
    if statutetu=="stagiaire" :
        convention = ConventionStage.objects.get(id=iddoc)
        form = SoutenanceStageForm(initial={'stage':convention})
        if request.method == 'POST':
            form = SoutenanceStageForm(request.POST)
            if form.is_valid():
                new_conv = form.save()
                return redirect('/arexbis/' + statut + '/' + id + '/')
    else :
        contrat = ContratApprentissage.objects.get(id=iddoc)
        form = SoutenanceApprentissageForm(initial={'apprentissage':contrat})
        if request.method == 'POST':
            form = SoutenanceApprentissageForm(request.POST)
            if form.is_valid():
                new_contr = form.save()
                return redirect('/arexbis/' + statut + '/' + id + '/')
    context = {"form" : form}
    return render(request, "ajoutsoutenance.html", context)

# dispo pour gestionnaires
def modifsoutenance(request: WSGIRequest, statut="", id="", statutetu="", idsout=""):
    match statutetu :
        case "stagiaire" :
            soutenance=SoutenanceStage.objects.get(id=idsout)
            form = SoutenanceStageForm(instance=soutenance)
            if request.method == 'POST':
                form = SoutenanceStageForm(request.POST, instance=soutenance)
                if form.is_valid():
                    new_sout = form.save()
                    return redirect('/arexbis/' + statut + '/' + id + '/')
        case "apprenti" :
            soutenance=SoutenanceApprentissage.objects.get(id=idsout)
            form = SoutenanceApprentissageForm(instance=soutenance)
            if request.method == 'POST':
                form = SoutenanceApprentissageForm(request.POST, instance=soutenance)
                if form.is_valid():
                    new_sout = form.save()
                    return redirect('/arexbis/' + statut + '/' + id + '/')
        case _:
            form=None
    context = {"form" : form}
    return render(request, "ajoutsoutenance.html", context)

# dispo pour gestionnaires
def suppsoutenance(request: WSGIRequest, statut="", id="", statutetu="", idsout=""):
    match statutetu :
        case "stagiaire" :
            SoutenanceStage.objects.get(id=idsout).delete()
        case "apprenti" :
            SoutenanceApprentissage.objects.get(id=idsout).delete()
    return redirect('/arexbis/' + statut + '/' + id + '/')

# dispo pour tous
def home(request: WSGIRequest, statut="", id=""):
    return render(request, "home.html", {"statut" : statut , "id" : id})

def connexion(request: WSGIRequest, statut=""):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            iduser = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=iduser, password=password)
            if user is not None:
                match statut:
                    case "stagiaire":
                        utilisateur = Stagiaire.objects.get(user=user)
                    case "apprenti":
                        utilisateur = Apprenti.objects.filter(user=user).first()
                    case "gestionnaire":
                        utilisateur = Gestionnaire.objects.filter(user=user).first()
                    case "tuteur":
                        utilisateur = Tuteur.objects.filter(user=user).first()
                    case "candide": 
                        utilisateur = Candide.objects.filter(user=user).first()
                    case "secretaire":
                        utilisateur = Secretaire.objects.get(user=user)
                    case _:
                        utilisateur = None
                if utilisateur is not None:
                    login(request, user)
                    return redirect("/arexbis/"+statut+"/"+iduser+"/")
    return render(request=request, template_name="login.html", context={'login_form': form})

def logout_request(request: WSGIRequest):
    logout(request)
    messages.info(request, "Vous vous êtes déconnecté avec succès")
    return redirect("/arexbis/")

def moncompte(request: WSGIRequest, id='', statut=""):
    user=User.objects.get(username=id)
    match statut :
        case "stagiaire":
            user=Stagiaire.objects.get(user=user)
        case "apprenti":
            user=Apprenti.objects.get(user=user)
        case "tuteur":
            user=Tuteur.objects.get(user=user)
        case "candide":
            user=Candide.objects.get(user=user)
        case "gestionnaire":
            user=Gestionnaire.objects.get(user=user)
        case "secretaire":
            user=Secretaire.objects.get(user=user)
    context={"user":user, "statut":statut}
    return render(request,'moncompte.html',context)

# form de création d'offre
# dispo pour gestionnaires et secrétaires
def posteroffre(request: WSGIRequest, statut="", id=""):
    form = OffreForm(initial={'datePubli':date.today()})
    if request.method == 'POST':
        form = OffreForm(request.POST)
        if form.is_valid():
            offre = form.save()
            return redirect("/arexbis/"+statut+"/"+id+"/")
    context = {'form': form}
    return render(request, "posteroffre.html", context)

# dispo pour gestionnaires et secrétaires
def modifoffre(request: WSGIRequest, statut="", id="", idoffre=""):
    data = Offre.objects.get(id=idoffre)
    form = OffreForm(instance = data)
    if request.method == 'POST':
        form = OffreForm(request.POST, instance=data)
        if form.is_valid():
            offre = form.save()
            return redirect("/arexbis/"+statut+"/"+id+"/")
    context = {'form': form}
    return render(request, "posteroffre.html", context)

# dispo pour gestionnaires et secrétaires
def suppoffre(request: WSGIRequest, statut="", id="", idoffre=""):
    Offre.objects.get(id=idoffre).delete()
    return redirect("/arexbis/"+statut+"/"+id+"/mesoffres/")

# afficher toutes les offres
def offres(request: WSGIRequest, id="", statut=""):
    offres=Offre.objects.all()
    if statut=="stagiaire" :
        offres=offres.filter(projet="stage")
    elif statut=="apprenti" :
        offres=offres.filter(projet="apprentissage")
    context={"offres":offres, "statut":statut, "iduser":id}
    return render(request,'mesoffres.html',context)

# afficher les offres de l'entreprise (lien cliquable à partir de l'annuaire)
def offresentreprise(request: WSGIRequest, id="", statut="", ident=""):
    entreprise = Entreprise.objects.get(id=ident)
    offres=Offre.objects.all(entreprise=entreprise)
    if statut=="stagiaire" :
        offres=offres.filter(projet="stage")
    elif statut=="apprenti" :
        offres=offres.filter(projet="apprentissage")
    context={"offres":offres, "statut":statut, "iduser":id}
    return render(request,'mesoffres.html',context)

# afficher l'offre en question 
def offre(request: WSGIRequest,id="",statut="",idoffre="") :
    offre=Offre.objects.get(id=idoffre)
    context={"iduser":id,"offre":offre, "statut":statut}
    return render(request,'offre.html',context)

# affiche tous les documents 
# apprenti : tous les apprentissages (passés et en cours)
# stagiaire : tous ses stages (passés et en cours), 
# tuteur : tous les stages/apprentissages dont il est tuteur en cours, 
# gestionnaire : tous les stages/apprentissages de son département, niveau & projet en cours (dont il est le gestionnaire)
# secretaires : tous les stages/apprentissages de son département en cours
def mesdocuments(request: WSGIRequest,id="", statut=""):
    conventionsEnCours=[]
    conventionsPassees=[]
    contratsEnCours=[]
    contratsPasses=[]
    user = User.objects.get(username=id)
    match statut :
        case "stagiaire" :
            user=Stagiaire.objects.get(user=user)
            conventions=ConventionStage.objects.all().filter(stagiaire=user)
            for conv in conventions :
                if docActuel(conv) : conventionsEnCours.append(conv)
                else : conventionsPassees.append(conv)
        case "apprenti" :
            user=Apprenti.objects.get(user=user)
            contrats=ContratApprentissage.objects.all().filter(apprenti=user)
            for contr in contrats :
                if docActuel(contr) : contratsEnCours.append(contr)
                else : contratsPasses.append(contr)
        case "tuteur" :
            user=Tuteur.objects.get(user=user)
            convs=ConventionStage.objects.all().filter(tuteur=user)
            for conv in convs :
                if docActuel(conv) : conventionsEnCours.append(conv)
            contrs=ContratApprentissage.objects.all().filter(tuteur=user)
            for contr in contrs :
                if docActuel(contr) : contratsEnCours.append(contr)
        case "gestionnaire" :
            user=Gestionnaire.objects.get(user=user)
            convs=ConventionStage.objects.all().filter(gestionnaire=user)
            for conv in convs :
                if docActuel(conv) : conventionsEnCours.append(conv)
            contrs = ContratApprentissage.objects.all().filter(gestionnaire=user)
            for contr in contrs :
                if docActuel(contr) : contratsEnCours.append(contr)
        case "secretaire":
            user=Secretaire.objects.get(user=user)
            stagiaires = Stagiaire.objects.all().filter(departement=user.departement)
            for stagiaire in stagiaires :
                convs = ConventionStage.objects.all().filter(stagiaire=stagiaire)
                for conv in convs :
                    if docActuel(conv) : conventionsEnCours.append(conv)
            apprentis = Apprenti.objects.all().filter(departement=user.departement)
            for apprenti in apprentis :
                contrs = ContratApprentissage.objects.all().filter(apprenti=apprenti)
                for contr in contrs :
                    if docActuel(contr) : contratsEnCours.append(contr)
    context={"iduser":id, 
             "conventionsEnCours":conventionsEnCours, 
             "conventionsPassees":conventionsPassees, 
             "contratsEnCours":contratsEnCours, 
             "contratsPasses":contratsPasses,
             "statut":statut}
    return render(request,'mesdocuments.html',context)

# affiche l'alternance en question (apprentis, tuteurs, gestionnaires)
def visucontrat(request: WSGIRequest,id="",statut="",idalternance=""):
    contrat=ContratApprentissage.objects.get(id=idalternance)
    context={"iduser":id,"contrat":contrat, "statut":statut}
    return render(request,'contrat.html',context)

def choixstatut(request: WSGIRequest):
    return render(request, 'choixstatut.html')

def choixsoutenance(request: WSGIRequest, id="", statut=""):
    context={"statut":statut, "iduser":id}
    return render(request, "choix_soutenance.html", context)

def listesoutenanceapprenti(request, id="", statut=""):
    soutenancesapprentissage=[]
    soutenances = SoutenanceApprentissage.objects.all()
    user = User.objects.get(username=id)
    if statut== "apprenti":
        apprenti=Apprenti.objects.get(user=user)
        contrats=ContratApprentissage.objects.all().filter(apprenti=apprenti)
        for sout in soutenances :
            if sout.apprentissage in contrats :
                soutenancesapprentissage.append(sout)
    elif statut == "tuteur":
        tuteur=Tuteur.objects.get(user=user)
        contrats=ContratApprentissage.objects.all().filter(tuteur=tuteur)
        for sout in soutenances :
            if sout.apprentissage in contrats :
                soutenancesapprentissage.append(sout)
    elif statut == "gestionnaire":
        gestionnaire=Gestionnaire.objects.get(user=user)
        contrats=ContratApprentissage.objects.all().filter(gestionnaire=gestionnaire)
        for sout in soutenances :
            if sout.apprentissage in contrats :
                soutenancesapprentissage.append(sout)
    elif statut == "candide":
        candide = Candide.objects.get(user=user)
        soutenancesapprentissage = soutenances.filter(candide=candide)
    context={"user":user, statut:True, "soutenancesapprentissage":   soutenancesapprentissage ,"statut":statut}
    return render(request,'listesoutenance_apprenti.html',context)

def listesoutenancestage(request, id="", statut=""):
    soutenancesstage=[]
    soutenances = SoutenanceStage.objects.all()
    user = User.objects.get(username=id)
    if statut== "stagiaire":
        stagiaire=Stagiaire.objects.get(user=user)
        conventions=ConventionStage.objects.all().filter(stagiaire=stagiaire)
        for sout in soutenances :
            if sout.stage in conventions :
                soutenancesstage.append(sout)
    elif statut == "tuteur":
        tuteur=Tuteur.objects.get(user=user)
        conventions=ConventionStage.objects.all().filter(tuteur=tuteur)
        for sout in soutenances :
            if sout.stage in conventions :
                soutenancesstage.append(sout)
    elif statut == "gestionnaire":
        gestionnaire=Gestionnaire.objects.get(user=user)
        conventions=ConventionStage.objects.all().filter(gestionnaire=gestionnaire)
        for sout in soutenances :
            if sout.stage in conventions :
                soutenancesstage.append(sout)
    elif statut == "candide":
        candide = Candide.objects.get(user=user)
        soutenancesstage = soutenances.filter(candide=candide)
    context={"user":user, statut:True, "soutenancesstage":soutenancesstage,"statut":statut}
    return render(request,'listesoutenance_stagiaire.html',context)

def creationpdf(request, idconv="",statut="",id=""):
    convention=ConventionStage.objects.get(id=idconv)
    template_path = 'pdf/pdf.html'
    context = {'convention':convention}
    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Convention.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)
    # create a pdf
    pisa_status = pisa.CreatePDF(
       html, dest=response )
    # if error then show some funny view
    if pisa_status.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response
