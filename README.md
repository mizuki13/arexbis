## PRESENTATION DU PROJET

Arexbis, plateforme en Pythn Django, réalisée par HUSTACHE Valentin, LAURENT Laora & SOMBOUTH Francis

Arexbis est une plateforme ayant pour but d'automatiser le gestion des stages et apprentissages des étudiants, de la création des conventions de stage à la gestion des soutenances.
6 rôles sont disponibles :
- Stagiaire
- Apprenti
- Tuteur (professeur responsable de certains stages / apprentissages)
- Gestionnaire (responsable des stages ou apprentissages d'un département et d'un niveau)
- Candide (professeur témoin lors de la soutenance)
- Secrétaire (le secrétariat du département)

## MISE EN PLACE DU PROJET

1. clonez le projet et placez-vous à sa racine
2. placez-vous dans votre venv
3. pour remplir la bd : 
    - python manage.py makemigrations arexbis
    - python manage.py migrate
    - python manage.py loaddata arexbis/fixtures/fixture.yaml
4. pour démarrer le serveur : python manage.py runserver
5. pour vider la db : python manage.py flush --noinput